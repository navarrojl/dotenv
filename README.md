# dotenv

These are the files and directories to configure my command line.

## My setup

Currently I am using the [dwm](https://dwm.suckless.org/) as window manager.  On top of it I apply the [tilegap](https://dwm.suckless.org/patches/tilegap/) patch.

### Related to the background

The background (wallpaper) is handled by [feh](https://wiki.archlinux.org/index.php/Feh) which relies on the `xcompmgr`, installed from `freeBSD` repositories.

## To do

Adding a fancy `dmenu` and `statusbar`.

